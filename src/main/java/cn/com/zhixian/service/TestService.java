package cn.com.zhixian.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.zhixian.dao.EmployeeMapper;
import cn.com.zhixian.model.Employee;



@Service("testService")
public class TestService {
 	private EmployeeMapper emplooyeeMapper;
	
	@Autowired
	public void setEmplooyeeMapper(EmployeeMapper emplooyeeMapper) {
		this.emplooyeeMapper = emplooyeeMapper;
	}

	public void testService(){
		System.out.println("I'am test service class");
		Employee employee=emplooyeeMapper.selectByPrimaryKey(1);
	    System.out.println(employee.getEmployeeName());	
	}

 
}
