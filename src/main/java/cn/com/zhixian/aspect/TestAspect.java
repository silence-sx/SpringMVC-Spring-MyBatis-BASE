package cn.com.zhixian.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TestAspect {
	private static final String pointCut = "execution(* cn.com.zhixian.service.*.*(..))";
	
	@Before(pointCut)
	public void before(JoinPoint joinPoint){
		System.out.println("what?");
	}
	
	@After(pointCut)
	public void after(Object obj){
		System.out.println(obj.getClass());
	}
}
