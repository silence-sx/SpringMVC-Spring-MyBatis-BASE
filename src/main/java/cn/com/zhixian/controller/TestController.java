package cn.com.zhixian.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.zhixian.service.TestService;

@Controller
public class TestController {
	private TestService testService;

     @Autowired
	public void setTestService(TestService testService) {
		this.testService = testService;
	}
	@RequestMapping("t1")
	public @ResponseBody void t1() {
		System.out.println("I'am test controller!!!");
		testService.testService();
	}
}
